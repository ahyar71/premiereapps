<?php

namespace App\Http\Controllers\APIX;

use App\Models\apps\kamar;
use App\Models\apps\keuangan;
use App\Models\apps\penghuni;
use App\Models\apps\User;
use App\Models\apps\model_datakos;
use App\Models\apps\pengguna;
use Illuminate\Http\Request;
use App\Helpers\ResponseFormatter;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use DateTime;
use Sentry\Util\JSON;

class FinanceController extends Controller
{
    public function all(Request $request)
    {
        $limit = $request->input('limit', 10);

        //DATA JUMLAH UANG TUNAI TERSEDIA
        $data_keuangan = keuangan::all()->where('statustransaksi', 'sudah')->where('idkost', 1);
        $totaluang = 0;
        $txuang = 0;
        foreach ($data_keuangan as $rowuang) {
            if ($rowuang->statustransaksi == 'sudah') {
                if ($rowuang->pilihantransaksi == "pemasukkan" && $rowuang->metode == "tunai") {
                    $totaluang += (int) $rowuang->nominaltransaksi + $rowuang->nominaltambahan;
                }
                if ($rowuang->pilihantransaksi == "pengeluaran" && $rowuang->metode == "tunai") {
                    $totaluang += (int) $rowuang->nominaltransaksi + $rowuang->nominaltambahan;
                }
            }
        }
        $txuang = $totaluang;

        //DATA KAMAR KOSONG
        $data_kamar = kamar::all()->where('idkost', 1);
        $tot_kosong = 0;
        $totalkamarterisi = 0;
        foreach ($data_kamar as $rowkamar) {
            if ($rowkamar->status == 'terisi') {
                $totalkamarterisi++;
            } else if ($rowkamar->status == 'kosong') {
                $tot_kosong++;
            }
        }

        $finance = keuangan::query()->where('statustransaksi', 'sudah')->where('idkost',1)->orderBy('updated_at', 'desc');

        $other = ['totaltunai' => $txuang, 'kosong' => $tot_kosong];

        // return ResponseFormatter::success($finance->paginate($limit),' Data list produk berhasil diambil');

        return ResponseFormatter::success($finance->paginate($limit), ' Data list produk berhasil diambil', $other);
    }

    public function jatuhtempo()
    {

        //DATA JUMLAH UANG TUNAI TERSEDIA
        $data_keuangan = keuangan::all()->where('idkost', 1);
        $totaluang = 0;
        $txuang = 0;
        foreach ($data_keuangan as $rowuang) {
            if ($rowuang->statustransaksi == 'sudah') {
                if ($rowuang->pilihantransaksi == "pemasukkan" && $rowuang->metode == "tunai") {
                    $totaluang += (int) $rowuang->nominaltransaksi + $rowuang->nominaltambahan;
                }
                if ($rowuang->pilihantransaksi == "pengeluaran" && $rowuang->metode == "tunai") {
                    $totaluang += (int) $rowuang->nominaltransaksi + $rowuang->nominaltambahan;
                }
            }
        }
        $txuang = $totaluang;

        //DATA KAMAR KOSONG
        $data_kamar = kamar::all()->where('idkost', 1);
        $tot_kosong = 0;
        $totalkamarterisi = 0;
        foreach ($data_kamar as $rowkamar) {
            if ($rowkamar->status == 'terisi') {
                $totalkamarterisi++;
            } else if ($rowkamar->status == 'kosong') {
                $tot_kosong++;
            }
        }

        $tanggalfrom = date('Y-m-d', strtotime('-7 day'));
        $tanggalto = date('Y-m-d', strtotime('3 day'));
        $other = ['totaltunai' => $txuang, 'kosong' => $tot_kosong];
        $jatuhtempoq = keuangan::where('statustransaksi', 'belumbayar')->where('idkost', '1')->whereBetween('jatuhtempo', [$tanggalfrom, $tanggalto,])->orderBy('jatuhtempo', 'desc')->get();


        foreach ($jatuhtempoq as $rowjatuhtempo) {
            $tanggalawal = date('Y-m-d', strtotime('now'));
            $datesawal = new DateTime($tanggalawal);
            $tanggalakhir = date('Y-m-d', strtotime($rowjatuhtempo->jatuhtempo));
            $datesakhir = new DateTime($tanggalakhir);
            $kamar = kamar::select('namakamarkost')->where('id', $rowjatuhtempo->idkamar)->get();
            $penghuni = penghuni::select('namapenghuni')->where('id', $rowjatuhtempo->idpenghuni)->get();
            foreach ($kamar as $rowkamar) {
                $rowjatuhtempo->namakamar = $rowkamar->namakamarkost;
            }
            foreach ($penghuni as $rowpenghuni) {
                $rowjatuhtempo->namapenghuni = $rowpenghuni->namapenghuni;
            }

            $a = date_diff($datesawal, $datesakhir)->format("%r%a");
            if ((strtotime("now") >= strtotime($rowjatuhtempo->jatuhtempo)) && (strtotime("-1 day") <= strtotime($rowjatuhtempo->jatuhtempo))) {
                $rowjatuhtempo->durasitempo = 0;
            } else {
                $rowjatuhtempo->durasitempo = $a;
            }

            if ($rowjatuhtempo->durasitempo >= 1) {
                $rowjatuhtempo->keterangantempo = $rowjatuhtempo->durasitempo . ' Hari lagi';
            } elseif ($rowjatuhtempo->durasitempo == 0) {
                $rowjatuhtempo->keterangantempo = $rowjatuhtempo->durasitempo . ' Hari lagi';
            } elseif ($rowjatuhtempo->durasitempo >= -1) {
                $rowjatuhtempo->keterangantempo = 'Lewat ' . $rowjatuhtempo->durasitempo . ' Hari';
            } elseif ($rowjatuhtempo->durasitempo >= -3) {
                $rowjatuhtempo->keterangantempo = 'Lewat ' . $rowjatuhtempo->durasitempo . ' Hari. DENDA';
            } elseif ($rowjatuhtempo->durasitempo >= -8) {
                $rowjatuhtempo->keterangantempo = 'Lewat ' . $rowjatuhtempo->durasitempo . ' Hari. DIKELUARKAN';
            }
        }
        $decoded['data'] = $jatuhtempoq;


        // return ResponseFormatter::success($finance->paginate($limit),' Data list produk berhasil diambil');

        return ResponseFormatter::success($decoded, ' Data list produk berhasil diambil', $other);
    }

    public function alltempo()
    {

        //DATA JUMLAH UANG TUNAI TERSEDIA
        $data_keuangan = keuangan::all()->where('idkost', 1);
        $totaluang = 0;
        $txuang = 0;
        foreach ($data_keuangan as $rowuang) {
            if ($rowuang->statustransaksi == 'sudah') {
                if ($rowuang->pilihantransaksi == "pemasukkan" && $rowuang->metode == "tunai") {
                    $totaluang += (int) $rowuang->nominaltransaksi + $rowuang->nominaltambahan;
                }
                if ($rowuang->pilihantransaksi == "pengeluaran" && $rowuang->metode == "tunai") {
                    $totaluang += (int) $rowuang->nominaltransaksi + $rowuang->nominaltambahan;
                }
            }
        }
        $txuang = $totaluang;

        //DATA KAMAR KOSONG
        $data_kamar = kamar::all()->where('idkost', 1);
        $tot_kosong = 0;
        $totalkamarterisi = 0;
        foreach ($data_kamar as $rowkamar) {
            if ($rowkamar->status == 'terisi') {
                $totalkamarterisi++;
            } else if ($rowkamar->status == 'kosong') {
                $tot_kosong++;
            }
        }

        $other = ['totaltunai' => $txuang, 'kosong' => $tot_kosong];
        $jatuhtempoq = keuangan::where('statustransaksi', 'belumbayar')->where('idkost', '1')->orderBy('jatuhtempo', 'desc')->get();


        foreach ($jatuhtempoq as $rowjatuhtempo) {
            $tanggalawal = date('Y-m-d', strtotime('now'));
            $datesawal = new DateTime($tanggalawal);
            $tanggalakhir = date('Y-m-d', strtotime($rowjatuhtempo->jatuhtempo));
            $datesakhir = new DateTime($tanggalakhir);
            $kamar = kamar::select('namakamarkost')->where('id', $rowjatuhtempo->idkamar)->get();
            $penghuni = penghuni::select('namapenghuni')->where('id', $rowjatuhtempo->idpenghuni)->get();
            foreach ($kamar as $rowkamar) {
                $rowjatuhtempo->namakamar = $rowkamar->namakamarkost;
            }
            foreach ($penghuni as $rowpenghuni) {
                $rowjatuhtempo->namapenghuni = $rowpenghuni->namapenghuni;
            }

            $a = date_diff($datesawal, $datesakhir)->format("%r%a");
            if ((strtotime("now") >= strtotime($rowjatuhtempo->jatuhtempo)) && (strtotime("-1 day") <= strtotime($rowjatuhtempo->jatuhtempo))) {
                $rowjatuhtempo->durasitempo = 0;
            } else {
                $rowjatuhtempo->durasitempo = $a;
            }
        }
        $decoded['data'] = $jatuhtempoq;


        // return ResponseFormatter::success($finance->paginate($limit),' Data list produk berhasil diambil');

        return ResponseFormatter::success($decoded, ' Data list produk berhasil diambil', $other);
    }

    public function create(Request $request)
    {
        try {
            $arr = [];
            
            $idkamar = penghuni::where('id', $request['idpenghuni'])->value('idkamar'); //m
            $hargakamar = kamar::where('id', $idkamar)->value('harga');

            //Validasi form Pemasukkan
            if ($request['pilihantransaksi'] == 'pemasukkan' && $request['jenistransaksi'] == 'dp' || $request['jenistransaksi'] == 'bulanan') {
                $arr = [
                    'lamasewa' => ['required', 'numeric', 'min:1'],
                    'idpenghuni' => ['required'],
                    'tanggaltempo' => ['required', 'date'],
                ];
            } else if ($request['pilihantransaksi'] == 'pemasukkan' && $request['jenistransaksi'] == 'lunas') {
                $arr = [
                    'jenislunas' => ['required'],
                    'idpenghuni' => ['required'],
                    'tanggaltempo' => ['required', 'date'],
                    'nominal' => ['size:'.$request['nominal']*$hargakamar],
                ];
            } else if ($request['pilihantransaksi'] == 'pemasukkan' && $request['jenistransaksi'] == 'lunaslangsung') {
                $arr = [
                    'lamasewa' => ['required', 'numeric', 'min:1'],
                    'jenislunas' => ['required'],
                    'idpenghuni' => ['required'],
                    'tanggaltempo' => ['required', 'date'],
                ];
            } else {
                $arr = [];
            }


            $request->validate([
                'metode' => ['required', 'string'],
                'pilihantransaksi' => ['required', 'string'],   
                'jenistransaksi' => ['required', 'string'],
                'nominal' => ['required', 'numeric', 'min:99'],
            ] . array_push($arr));

            $datakos = model_datakos::select('id')->where('idpenjaga', Auth::user()->id)->get();
            $penginput = '';
            $pilihantransaksi = '';
            $jenistransaksi = '';
            foreach ($datakos as $rowkost) {

                $idpenghuni = $request['listdp'];
                $idkamar = penghuni::where('id', $idpenghuni)->value('idkamar'); //mengambil id penghuni kamar 
                $namapenghuni = penghuni::where('id', $idpenghuni)->value('namapenghuni');
                $namakamar = kamar::where('id', $idkamar)->value('namakamarkost');
                $tambahanbiaya = 0;
                if ($request['tipe'] == 'sendiri') {
                    $tambahanbiaya = 0;
                } else if ($request['tipe'] == 'pasutri') {
                    $tambahanbiaya = 200000;
                } else if ($request['tipe'] == 'adikkakak') {
                    $tambahanbiaya = 200000;
                } else if ($request['tipe'] == 'teman') {
                    $tambahanbiaya = 200000;
                }

                if ($request['pilihantransaksi'] == 'pemasukkan' && $request['jenistransaksi'] == 'dp') {
                    if ($request['lamasewa'] <= 0) {
                        return back()->with('error', 'Maaf, Lama Sewa Minimal 1 Bulan');
                    }
                    $idpenghuni = $request['listdp'];
                    $idkamar = penghuni::where('id', $idpenghuni)->value('idkamar'); //mengambil id penghuni kamar 
                    $namapenghuni = penghuni::where('id', $idpenghuni)->value('namapenghuni');
                    $namakamar = kamar::where('id', $idkamar)->value('namakamarkost');
                    $jenistransaksi = $request['jenistransaksi'];
                    $pilihantransaksi = $request['pilihantransaksi'];
                    $nominal = $request['nominaltransaksi'];
                    $metode = $request['metode'];
                    $keterangan = "Pembayaran DP " . $namapenghuni . " (Kamar " . $namakamar . " ) || Catatan: " . $request['keterangantransaksi'];
                    $tanggal = $request['tanggaltransaksi'];
                    $tanggaltransaksi = date('Y-m-d', strtotime($request['tanggaltransaksi']));
                    $status = 'sudah';
                    $penginput = pengguna::where('id', Auth::user()->id)->value('nama');
                    $jatuhtempo = date('Y-m-d', strtotime($request['tanggaltempo']));


                    if ($nominal < 200000 || $nominal > 800000) {
                        return back()->with('error', 'Nominal DP Minimal Rp. 200.000 dan Maksimal Rp. 800.000');
                    }

                    $this->firebase_push($penginput, $pilihantransaksi, $jenistransaksi, $nominal);
                    //buat data dp
                    $data = array('idpenghuni' => $idpenghuni, 'idkost' => $rowkost->id, 'idkamar' => $idkamar, 'jenistransaksi' => $jenistransaksi, 'nominaltransaksi' => $nominal, 'pilihantransaksi' => $pilihantransaksi, 'statustransaksi' => $status, 'metode' => $metode, 'tanggaltransaksi' => $tanggaltransaksi, 'jatuhtempo' => $jatuhtempo, 'deskripsi' => $keterangan, 'penginput' => $penginput);
                    keuangan::create($data);

                    //buat data pelunasan            
                    $keterangan = "Pembayaran Lunas " . $namapenghuni . " (Kamar " . $namakamar . " ) || Catatan: " . $request['keterangantransaksi'];
                    $idpenghuni = $request['listdp'];
                    $lamasewa = $request['lamasewa'];
                    $status = 'belumbayar';
                    $jenistransaksi = 'lunas';
                    $idkamar = penghuni::where('id', $idpenghuni)->value('idkamar'); //mengambil id penghuni kamar   
                    $hargakamar = kamar::where('id', $idkamar)->value('harga');
                    $dilunasi = ($hargakamar * $lamasewa + $tambahanbiaya) - $nominal;
                    if ($nominal == 200000) {
                        $jatuhtempo = date('Y-m-d', strtotime("+8 days"));
                    } else if ($nominal > 200000 && $nominal <= 800000) {
                        $jatuhtempo = date('Y-m-d', strtotime("+14 days"));
                    }
                    $data2 = array('idpenghuni' => $idpenghuni, 'idkost' => $rowkost->id, 'idkamar' => $idkamar, 'jenistransaksi' => $jenistransaksi, 'nominaltransaksi' => $dilunasi, 'pilihantransaksi' => $pilihantransaksi, 'statustransaksi' => $status, 'tanggaltransaksi' => $tanggaltransaksi, 'jatuhtempo' => $jatuhtempo, 'deskripsi' => $keterangan, 'penginput' => $penginput);
                    keuangan::create($data2);

                    return redirect('/keuangan')->with('success', 'Data Pemasukkan Pembayaran DP Berhasil dimasukkan!');
                } else if ($request['pilihantransaksi'] == 'pemasukkan' && $request['jenislunas'] == 'lunas') {
                    $nominal = $request['nominaltransaksi'];
                    $idtransaksi = $request['listlunas'];
                    $metode = $request['metode'];
                    $datakeuangan = keuangan::find($idtransaksi);
                    $idpenghuni = keuangan::where('id', $idtransaksi)->value('idpenghuni');
                    $idkamar = keuangan::where('id', $idtransaksi)->value('idkamar');
                    $nominal2 = keuangan::where('id', $idtransaksi)->value('nominaltransaksi');
                    $hargakamar = kamar::where('id', $idkamar)->value('harga');
                    $namapenghuni = penghuni::where('id', $idpenghuni)->value('namapenghuni');
                    $namakamar = kamar::where('id', $idkamar)->value('namakamarkost');
                    $idtransaksidp = keuangan::where('idkamar', $idkamar)->where('idpenghuni', $idpenghuni)->where('jenistransaksi', 'dp')->where('statustransaksi', 'sudah')->value('id');
                    $nominaltambah = keuangan::where('id', $idtransaksidp)->value('nominaltambahan');
                    $nominal1 = keuangan::where('id', $idtransaksidp)->value('nominaltransaksi');
                    $totalnominal = $nominal1 + $nominal2 - $nominaltambah;
                    $lamasewa = $totalnominal / $hargakamar;
                    $jatuhtempo = date('Y-m-d', strtotime($request['tanggaltempo']));

                    if ($nominal != $nominal2) {
                        return back()->with('error', 'Data Tidak Berhasil Dimasukkan, Nominal Pelunasan Harus Rp. ' . number_format($nominal2));
                    }

                    if ((strtotime("-60 day") <= strtotime($request['tanggaltempo'])) && (strtotime("+5 days") >= strtotime($request['tanggaltempo']))) {
                        return back()->with('error', 'Mbak Cuma bisa Masukin Tanggal Tempo yang ngga terlalu jauh dari tanggal yang seharusnya');
                    }


                    $pilihantransaksi = $request['pilihantransaksi'];
                    $keterangan = "Pembayaran Lunas " . $namapenghuni . " (Kamar " . $namakamar . " ) || Catatan: " . $request['keterangantransaksi'];
                    $tanggaltransaksi = date('Y-m-d', strtotime($request['tanggaltransaksi']));
                    $status = 'sudah';

                    $datakeuangan->nominaltransaksi = $nominal;
                    $datakeuangan->jatuhtempo = $jatuhtempo;
                    $datakeuangan->deskripsi = $keterangan;
                    $datakeuangan->tanggaltransaksi = $tanggaltransaksi;
                    $datakeuangan->statustransaksi = $status;
                    $datakeuangan->metode = $metode;
                    $datakeuangan->save();

                    $this->firebase_push($penginput, $pilihantransaksi, $jenistransaksi, $nominal);
                    //buat data pelunasan
                    $keterangan = "Pembayaran Bulanan " . $namapenghuni . " (Kamar " . $namakamar . " ) || Catatan: " . $request['keterangantransaksi'];
                    $bayardp = keuangan::where('idpenghuni', $idpenghuni)->where('jenistransaksi', 'dp')->value('nominaltransaksi');
                    $bayarlunas = keuangan::where('idpenghuni', $idpenghuni)->where('jenistransaksi', 'lunas')->value('nominaltransaksi');
                    $tambahbayar = $bayardp + $bayarlunas;
                    $lamalanjut = 1;
                    $status = 'belumbayar';
                    $jenistransaksi = 'bulanan';
                    $belumlunas = $hargakamar * $lamalanjut;
                    $jatuhtempo = date($request['tanggaltempo']);
                    $penginput = pengguna::where('id', Auth::user()->id)->first()->nama;


                    $data2 = array('idpenghuni' => $idpenghuni, 'idkost' => $rowkost->id, 'idkamar' => $idkamar, 'jenistransaksi' => $jenistransaksi, 'nominaltransaksi' => $tambahbayar, 'pilihantransaksi' => $pilihantransaksi, 'statustransaksi' => $status, 'tanggaltransaksi' => $tanggaltransaksi, 'jatuhtempo' => $jatuhtempo, 'deskripsi' => $keterangan, 'penginput' => $penginput);
                    keuangan::create($data2);

                    return redirect('/keuangan')->with('success', 'Data Pemasukkan Pelunasan Dengan DP Berhasil dimasukkan!');
                } elseif ($request['pilihantransaksi'] == 'pemasukkan' && $request['jenislunas'] == 'lunaslangsung') {
                    if ($request['lamasewa'] <= 0) {
                        return back()->with('error', 'Maaf, Lama Sewa Minimal 1 Bulan');
                    }
                    $idpenghuni = $request['listlangsunglunas'];
                    $idkamar = penghuni::where('id', $idpenghuni)->value('idkamar'); //mengambil id penghuni kamar 
                    $namapenghuni = penghuni::where('id', $idpenghuni)->value('namapenghuni');
                    $namakamar = kamar::where('id', $idkamar)->value('namakamarkost');
                    $hargakamar = kamar::where('id', $idkamar)->value('harga');
                    $lamasewa = $request['lamasewa'];
                    $nominalharusdibayar = ($lamasewa * $hargakamar) + $tambahanbiaya;
                    $jenistransaksi = $request['jenislunas'];
                    $pilihantransaksi = $request['pilihantransaksi'];
                    $nominal = $request['nominaltransaksi'];
                    $metode = $request['metode'];
                    $keterangan = "Pembayaran Lunas Langsung " . $namapenghuni . " Bulan " . date("F", strtotime("now")) . " Hingga Bulan " . date("F", strtotime("now" . " +" . $lamasewa . " month")) . "  (Kamar " . $namakamar . " ) || Catatan: " . $request['keterangantransaksi'];
                    $tanggal = $request['tanggaltransaksi'];
                    $tanggaltransaksi = date('Y-m-d', strtotime($request['tanggaltransaksi']));
                    $status = 'sudah';
                    $penginput = pengguna::where('id', Auth::user()->id)->value('nama');
                    $jatuhtempo = date('Y-m-d', strtotime($request['tanggaltempo']));

                    if ($nominal < $nominalharusdibayar || $nominal > $nominalharusdibayar) {
                        return back()->with('error', 'Nominal Pembayaran Lunas Langsung Harus Rp. ' . number_format($nominalharusdibayar));
                    }

                    $this->firebase_push($penginput, $pilihantransaksi, $jenistransaksi, $nominal);
                    //buat data dp
                    $data = array('idpenghuni' => $idpenghuni, 'idkost' => $rowkost->id, 'idkamar' => $idkamar, 'jenistransaksi' => $jenistransaksi, 'nominaltransaksi' => $nominalharusdibayar, 'pilihantransaksi' => $pilihantransaksi, 'statustransaksi' => $status, 'tanggaltransaksi' => $tanggaltransaksi, 'jatuhtempo' => $jatuhtempo, 'deskripsi' => $keterangan, 'metode' => $metode, 'penginput' => $penginput);
                    keuangan::create($data);

                    //buat data pelunasan
                    $keterangan = "Pembayaran Bulanan " . $namapenghuni . " (Kamar " . $namakamar . " ) || Catatan: " . $request['keterangantransaksi'];
                    $lamalanjut = $request['lamasewa'];
                    $hargakamar = kamar::where('id', $idkamar)->value('harga');
                    $status = 'belumbayar';
                    $jenistransaksi = 'bulanan';
                    $belumlunas = $hargakamar * $lamalanjut;
                    $penginput = pengguna::where('id', Auth::user()->id)->first()->nama;

                    $data2 = array('idpenghuni' => $idpenghuni, 'idkost' => $rowkost->id, 'idkamar' => $idkamar, 'jenistransaksi' => $jenistransaksi, 'nominaltransaksi' => $belumlunas, 'pilihantransaksi' => $pilihantransaksi, 'nominaltambahan' => null, 'statustransaksi' => $status, 'tanggaltransaksi' => $tanggaltransaksi, 'jatuhtempo' => $jatuhtempo, 'deskripsi' => $keterangan, 'penginput' => $penginput);
                    keuangan::create($data2);
                } else if ($request['pilihantransaksi'] == 'pemasukkan' && $request['jenistransaksi'] == 'bulanan') {

                    $idtransaksi = $request['listbulanan'];
                    $datakeuangan = keuangan::find($idtransaksi);
                    $idpenghuni = keuangan::where('id', $idtransaksi)->value('idpenghuni');
                    $tambahanbiaya += keuangan::where('id', $idtransaksi)->value('nominaltambahan');
                    $idkamar = keuangan::where('id', $idtransaksi)->value('idkamar');
                    $namapenghuni = penghuni::where('id', $idpenghuni)->value('namapenghuni');
                    $namakamar = kamar::where('id', $idkamar)->value('namakamarkost');
                    $nominal = $request['nominaltransaksi'];
                    $metode = $request['metode'];
                    $hargakamar = kamar::where('id', $idkamar)->value('harga');
                    $lamasewa = $request['lamasewa'];
                    $nominaltanpatambahan = ($hargakamar * $lamasewa);
                    $jatuhtempoawal = keuangan::where('id', $idtransaksi)->value('jatuhtempo');

                    $pilihantransaksi = $request['pilihantransaksi'];
                    $keterangan = "Pembayaran Bulanan " . $namapenghuni . " Bulan " . date("F", strtotime($jatuhtempoawal)) . " Hingga Bulan " . date("F", strtotime($jatuhtempoawal . " +" . $lamasewa . " month")) . " (Kamar " . $namakamar . " ) || Catatan: " . $request['keterangantransaksi'];
                    $tanggal = $request['tanggaltransaksi'];
                    $tanggaltransaksi = date('Y-m-d', strtotime($request['tanggaltransaksi']));
                    $status = 'sudah';

                    $datakeuangan->nominaltransaksi = $nominaltanpatambahan;
                    $datakeuangan->nominaltambahan = $tambahanbiaya;
                    $datakeuangan->deskripsi = $keterangan;
                    $datakeuangan->tanggaltransaksi = $tanggaltransaksi;
                    $datakeuangan->statustransaksi = $status;
                    $datakeuangan->metode = $metode;
                    $datakeuangan->save();

                    $this->firebase_push($penginput, $pilihantransaksi, $jenistransaksi, $nominal);

                    //buat data pelunasan            
                    if ($request['tipe'] == 'sendiri') {
                        $tambahanbiaya = 0;
                    } else if ($request['tipe'] == 'pasutri') {
                        $tambahanbiaya = 200000;
                    } else if ($request['tipe'] == 'adikkakak') {
                        $tambahanbiaya = 200000;
                    } else if ($request['tipe'] == 'teman') {
                        $tambahanbiaya = 200000;
                    }
                    $keterangan = "Pembayaran Bulanan " . $namapenghuni . " (Kamar " . $namakamar . " ) || Catatan: " . $request['keterangantransaksi'];
                    $lamalanjut = $request['lamasewa'];
                    $hargakamar = kamar::where('id', $idkamar)->value('harga');
                    $status = 'belumbayar';
                    $jatuhtempoawal = keuangan::where('id', $idtransaksi)->value('jatuhtempo');
                    $jenistransaksi = 'bulanan';
                    $belumlunas = $hargakamar;
                    $jatuhtempoakhir = date("Y-m-d", strtotime($request['tanggaltempo']));
                    $penginput = pengguna::where('id', Auth::user()->id)->first()->nama;

                    $data2 = array('idpenghuni' => $idpenghuni, 'idkost' => $rowkost->id, 'idkamar' => $idkamar, 'jenistransaksi' => $jenistransaksi, 'nominaltransaksi' => $belumlunas, 'nominaltambahan' => null, 'pilihantransaksi' => $pilihantransaksi, 'statustransaksi' => $status, 'tanggaltransaksi' => $tanggaltransaksi, 'jatuhtempo' => $jatuhtempoakhir, 'deskripsi' => $keterangan, 'penginput' => $penginput);
                    keuangan::create($data2);
                } else if ($request['pilihantransaksi'] == 'pemasukkan' && $request['jenistransaksi'] == 'tambahan') {

                    list($jenis, $idtransaksi) = explode("|", $request['listtambahan']);
                    if ($jenis == 'baru') {
                        $idpenghuni = $idtransaksi;
                        $idkamar = penghuni::where('id', $idpenghuni)->value('idkamar'); //mengambil id penghuni kamar 
                        $namapenghuni = penghuni::where('id', $idpenghuni)->value('namapenghuni');
                        $tanggalmasuk = penghuni::where('id', $idpenghuni)->value('tanggalmasuk');
                        $namakamar = kamar::where('id', $idkamar)->value('namakamarkost');
                        $hargakamar = kamar::where('id', $idkamar)->value('harga');
                        $lamasewa = $request['lamahari'];

                        $jenistransaksi = 'tambahan';

                        $pilihantransaksi = $request['pilihantransaksi'];
                        $nominal = $request['nominaltransaksi'];
                        $metode = $request['metode'];
                        $keterangan = "Pembayaran  " . $lamasewa . " Hari " . $namapenghuni . " Bulan " . date("F", strtotime("now")) . " Hingga Bulan " . date("F", strtotime("now" . " +" . $lamasewa . " month")) . "  (Kamar " . $namakamar . " ) || Catatan: " . $request['keterangantransaksi'];
                        $tanggal = $request['tanggaltransaksi'];
                        $tanggaltransaksi = date('Y-m-d', strtotime($request['tanggaltransaksi']));
                        $status = 'sudah';
                        $penginput = pengguna::where('id', Auth::user()->id)->value('nama');

                        $jatuhtempo = date("Y-m-d", strtotime($tanggalmasuk . "+" . $lamasewa . "day"));

                        $this->firebase_push($penginput, $pilihantransaksi, $jenistransaksi, $nominal);
                        //buat data harian/mingguan
                        $data = array('idpenghuni' => $idpenghuni, 'idkost' => $rowkost->id, 'idkamar' => $idkamar, 'jenistransaksi' => $jenistransaksi, 'nominaltransaksi' => $nominal, 'pilihantransaksi' => $pilihantransaksi, 'statustransaksi' => $status, 'tanggaltransaksi' => $tanggaltransaksi, 'jatuhtempo' => $jatuhtempo, 'deskripsi' => $keterangan, 'metode' => $metode, 'penginput' => $penginput);
                        keuangan::create($data);

                        //buat data bulanan
                        $keterangan = "Pembayaran Selanjutnya " . $namapenghuni . " (Kamar " . $namakamar . " ) || Catatan: " . $request['keterangantransaksi'];
                        $lamalanjut = $request['lamahari'];
                        $hargakamar = kamar::where('id', $idkamar)->value('harga');
                        $status = 'belumbayar';
                        $jenistransaksi = 'bulanan';
                        $belumlunas = $hargakamar * $lamalanjut;
                        $penginput = pengguna::where('id', Auth::user()->id)->first()->nama;

                        $data2 = array('idpenghuni' => $idpenghuni, 'idkost' => $rowkost->id, 'idkamar' => $idkamar, 'jenistransaksi' => $jenistransaksi, 'nominaltransaksi' => $belumlunas, 'pilihantransaksi' => $pilihantransaksi, 'nominaltambahan' => null, 'statustransaksi' => $status, 'tanggaltransaksi' => $tanggaltransaksi, 'jatuhtempo' => $jatuhtempo, 'deskripsi' => $keterangan, 'penginput' => $penginput);
                        keuangan::create($data2);
                    } else {
                        $datakeuangan = keuangan::find($idtransaksi);
                        $idpenghuni = keuangan::where('id', $idtransaksi)->value('idpenghuni');
                        $tambahanbiaya += keuangan::where('id', $idtransaksi)->value('nominaltambahan');
                        $idkamar = keuangan::where('id', $idtransaksi)->value('idkamar');
                        $namapenghuni = penghuni::where('id', $idpenghuni)->value('namapenghuni');
                        $namakamar = kamar::where('id', $idkamar)->value('namakamarkost');
                        $nominal = $request['nominaltransaksi'];
                        $metode = $request['metode'];
                        $lamasewa = $request['lamahari'];
                        $jatuhtempoawal = keuangan::where('id', $idtransaksi)->value('jatuhtempo');


                        $pilihantransaksi = $request['pilihantransaksi'];
                        $keterangan = "Pembayaran Lanjutan " . $namapenghuni . " " . $lamasewa . " Hari (Kamar " . $namakamar . " ) || Catatan: " . $request['keterangantransaksi'];
                        $tanggal = $request['tanggaltransaksi'];
                        $tanggaltransaksi = date('Y-m-d', strtotime($request['tanggaltransaksi']));
                        $status = 'sudah';
                        $jenistransaksi = 'tambahan';


                        $datakeuangan->jenistransaksi = $jenistransaksi;
                        $datakeuangan->nominaltransaksi = $nominal;
                        $datakeuangan->nominaltambahan = 0;
                        $datakeuangan->deskripsi = $keterangan;
                        $datakeuangan->tanggaltransaksi = $tanggaltransaksi;
                        $datakeuangan->statustransaksi = $status;
                        $datakeuangan->metode = $metode;
                        $datakeuangan->save();

                        $this->firebase_push($penginput, $pilihantransaksi, $jenistransaksi, $nominal);
                        $lamalanjut = $request['lamahari'];
                        $keterangan = "Pembayaran Lanjutan " . $namapenghuni . " " . $lamasewa . " Hari (Kamar " . $namakamar . " ) || Catatan: " . $request['keterangantransaksi'];
                        $hargakamar = kamar::where('id', $idkamar)->value('harga');
                        $status = 'belumbayar';
                        $jatuhtempoawal = keuangan::where('id', $idtransaksi)->value('jatuhtempo');
                        $jenistransaksi = 'bulanan';
                        $jatuhtempoakhir = date("Y-m-d", strtotime($jatuhtempoawal . "+" . $lamalanjut . "day"));
                        $penginput = pengguna::where('id', Auth::user()->id)->first()->nama;

                        $data2 = array('idpenghuni' => $idpenghuni, 'idkost' => $rowkost->id, 'idkamar' => $idkamar, 'jenistransaksi' => $jenistransaksi, 'nominaltransaksi' => 0, 'nominaltambahan' => null, 'pilihantransaksi' => $pilihantransaksi, 'statustransaksi' => $status, 'tanggaltransaksi' => $tanggaltransaksi, 'jatuhtempo' => $jatuhtempoakhir, 'deskripsi' => $keterangan, 'penginput' => $penginput);
                        keuangan::create($data2);
                    }
                } else if ($request['pilihantransaksi'] == 'pemasukkan' && $request['jenistransaksi'] == 'lain') {

                    $nominal = $request['nominaltransaksi'];
                    $metode = $request['metode'];

                    $pilihantransaksi = $request['pilihantransaksi'];
                    $tanggal = $request['tanggaltransaksi'];
                    $tanggaltransaksi = date('Y-m-d', strtotime($request['tanggaltransaksi']));
                    $keterangan = "Pembayaran Lainnya || Catatan: " . $request['keterangantransaksi'];
                    $status = 'sudah';
                    $jenistransaksi = 'lainnya';
                    $penginput = pengguna::where('id', Auth::user()->id)->first()->nama;

                    $this->firebase_push($penginput, $pilihantransaksi, $jenistransaksi, $nominal);
                    $data2 = array('jenistransaksi' => $jenistransaksi, 'idkost' => $rowkost->id, 'nominaltransaksi' => $nominal, 'nominaltambahan' => null, 'pilihantransaksi' => $pilihantransaksi, 'statustransaksi' => $status, 'metode' => $metode, 'tanggaltransaksi' => $tanggaltransaksi, 'jatuhtempo' => null, 'deskripsi' => $keterangan, 'penginput' => $penginput);
                    keuangan::create($data2);
                } else if ($request['pilihantransaksi'] == 'pengeluaran') {

                    $nominal = "-" . $request['nominaltransaksi'];
                    $idkamar = null;
                    $idpenghuni = null;
                    $jenistransaksi = $request['jenistransaksi'];
                    $pilihantransaksi = $request['pilihantransaksi'];
                    $statustransaksi = 'sudah';
                    $jatuhtempo = null;
                    $metode = $request['metode'];
                    $deskripsi = "Pembayaran " . $request['jenistransaksi'] . ' || Catatan: ' . $request['keterangantransaksi'];
                    $tanggaltransaksi = date('Y-m-d', strtotime("now"));
                    $penginput = pengguna::where('id', Auth::user()->id)->first()->nama;

                    $data2 = array('idpenghuni' => $idpenghuni, 'idkost' => $rowkost->id, 'idkamar' => $idkamar, 'jenistransaksi' => $jenistransaksi, 'nominaltransaksi' => $nominal, 'pilihantransaksi' => $pilihantransaksi, 'nominaltambahan' => $tambahanbiaya, 'statustransaksi' => $statustransaksi, 'tanggaltransaksi' => $tanggaltransaksi, 'jatuhtempo' => $jatuhtempo, 'metode' => $metode, 'deskripsi' => $deskripsi, 'penginput' => $penginput);
                    keuangan::create($data2);


                    $this->firebase_push($penginput, $pilihantransaksi, $jenistransaksi, $nominal);
                }
            }


            return ResponseFormatter::success([], 'Data ' . $request['pilihantransaksi'] . ' ' . $request['jenistransaksi'] . ' berhasil dimasukkan!');
        } catch (\Throwable $e) {
            return ResponseFormatter::error([
                'message' => 'Something went wrong',
                'error' => $e
            ], 'Data Input Failed!', 500);
        }
    }
}
