<?php

namespace App\Http\Controllers\APIX;

use App\Models\apps\kamar;
use App\Models\apps\keuangan;
use App\Models\apps\penghuni;
use App\Models\apps\User;
use App\Models\apps\model_datakos;
use App\Models\apps\pengguna;
use Illuminate\Http\Request;
use App\Helpers\ResponseFormatter;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use DateTime;

class KamarController extends Controller
{
    public function all(Request $request)
    {


        $finance = kamar::query()->where('idkost', 1)->orderBy('id', 'asc')->get();

        // return ResponseFormatter::success($finance->paginate($limit),' Data list produk berhasil diambil');

        return ResponseFormatter::success($finance, ' Data list produk berhasil diambil');
    }
}
