<?php

namespace App\Models;


use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class penghuni extends Model
{
    use HasFactory;

    protected $table = 'penghuni';
    
    protected $fillable = ['username', 'tag', 'namapenghuni', 'idkost', 'idkamar', 'nik', 'email', 'nopenghuni', 'pekerjaan', 'jeniskendaraan', 'platkendaraan', 'tanggalmasuk', 'deskripsi', 'fotonik', 'created_at', 'updated_at'];

}
