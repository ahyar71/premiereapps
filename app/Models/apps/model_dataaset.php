<?php

namespace App\Models\apps;

use Illuminate\Database\Eloquent\Model;

class model_dataaset extends Model
{
    protected $table = 'data_aset';

    protected $fillable = ['id','idkost','idkamar','nama','waktuservis','waktupembelian','jangkaservice','updated_at','created_at'];

    public function getNamaKost()
		{	
		    $namakost = model_datakos::where('id',$this->idkost)->value('kost');
			return $namakost;
		}

	public function getNamaKamar()
		{	
		    $namakamarkost = kamar::where('id',$this->idkamar)->value('namakamarkost');
			return $namakamarkost;
		}
}
