<?php

namespace App\Models\apps;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class kamar extends Model
{
    use HasFactory;
    
    protected $table = 'kamar';

    protected $fillable = ['namakamarkost', 'idkost', 'harga', 'status', 'namapenghuni', 'kasur', 'kmdalam', 'mejakursi', 'lemari', 'spreibantal', 'ac', 'created_at', 'updated_at'];

}
