<?php

namespace App\Models\apps;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class keuangan extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'keuangan';
    
    protected $fillable = ['idpenghuni','idkost','idkamar','jenistransaksi','nominaltransaksi','nominaltambahan','tanggaltransaksi','statustransaksi','pilihantransaksi','jatuhtempo','metode','deskripsi','penginput','penginput','created_at','updated_at'];




    public function getCreatedAtAttribute($value)
    {
        return Carbon::parse($value)->timestamp;
    }

    public function getUpdatedAtAttribute($value)
    {
        return Carbon::parse($value)->timestamp;
    }
    


}
