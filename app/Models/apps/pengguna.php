<?php

namespace App\Models\apps;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class pengguna extends Authenticatable 
{	
	protected $table='pengguna';
	
    protected $fillable = [
        'id','idkost', 'nama','username','password','isAdmin'];

}
