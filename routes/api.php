<?php

use App\Http\Controllers\API\FoodController;
use App\Http\Controllers\API\MidtransController;
use App\Http\Controllers\API\TransactionController;
use App\Http\Controllers\API\UserController;
use App\Http\Controllers\APIX\FinanceController;
use App\Http\Controllers\APIX\KamarController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|tess
*/

Route::middleware('auth:sanctum')->group(function(){
    Route::get('user', [UserController::class ,'fetch']);
    Route::post('user', [UserController::class ,'updateProfile']);
    Route::post('user/photo', [UserController::class ,'updatePhoto']);
    Route::post('logout', [UserController::class ,'logout']);

    Route::post('checkout', [TransactionController::class ,'checkout']);

    Route::get('transaction', [TransactionController::class ,'all']);
    Route::post('transaction/{id}', [TransactionController::class ,'update']);
});

Route::post('login', [UserController::class ,'login']);
Route::post('register', [UserController::class ,'register']);

Route::post('createfood', [FoodController::class ,'createfood']);


Route::get('food', [FoodController::class ,'all']);

Route::get('finance', [FinanceController::class ,'all']);
Route::get('fjatuhtempo', [FinanceController::class ,'jatuhtempo']);


Route::get('kamar', [KamarController::class ,'all']);

//url callback midtrans masukan ke dashboard midtransnya nanti
Route::post('midtrans/callback', [MidtransController::class ,'callback']);